package main

import (
	"fmt"
	"log"
	"os/exec"
	"path"
	"path/filepath"
	"strings"

	"bitbucket.org/_metalogic_/gogit"
)

func walk(dirname string, te *gogit.TreeEntry) int {
	log.Println(path.Join(dirname, te.Name))
	return 0
}

func main() {
	cmdOut, err := exec.Command("git", "rev-parse", "--show-toplevel").Output()
	if err != nil {
		log.Fatalf("Error getting the git base path: %s - %s", err.Error(), string(cmdOut))
	}
	base := strings.TrimSpace(string(cmdOut))

	fmt.Println("base: " + base)

	repository, err := gogit.OpenRepository(filepath.Join(base, ".git"))
	if err != nil {
		log.Fatal(err)
	}
	ref, err := repository.LookupReference("HEAD")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("ref: ", ref)

	ci, err := repository.LookupCommit(ref.Oid)
	if err != nil {
		log.Fatal(err)
	}
	ci.Tree.Walk(walk)
}
